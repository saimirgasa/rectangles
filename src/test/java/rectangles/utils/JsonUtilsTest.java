package rectangles.utils;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Test;

import rectangles.Intersections;
import rectangles.dto.Point;
import rectangles.dto.Rectangle;
import rectangles.enums.FileType;
import rectangles.interfaces.IRectangle;

public class JsonUtilsTest {

    @Test(expected = ParseException.class)
    public void readMalformedJSONFile() throws IOException, ParseException {
        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/bad_json_file.json", FileType.RESOURCE, 10);
    }

    @Test(expected = AssertionError.class)
    public void readNonExistentJSONFile() throws IOException, ParseException {
        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/non_existent_json_file.json", FileType.RESOURCE, 10);
    }

    @Test
    public void readJSONFile() throws IOException, ParseException {
        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/intersections_between_two_rectangles.json", FileType.RESOURCE, 10);

        IRectangle firstRectangle = new Rectangle(
            1,
            new Point(100, 100),
            250,
            80,
            new TreeSet<>()
        );

        IRectangle secondRectangle = new Rectangle(
            1,
            new Point(120, 200),
            250,
            150,
            new TreeSet<>()
        );

        IRectangle thirdRectangle = new Rectangle(
            1,
            new Point(60, 60),
            50,
            50,
            new TreeSet<>()
        );

        List<IRectangle> expectedRectangleList =
            new ArrayList<>(List.of(firstRectangle, secondRectangle, thirdRectangle));

        assertEquals(expectedRectangleList, rectangleList);
    }

    @Test
    public void parseRectangleObject() throws IOException, ParseException {
        JSONArray jsonArray = new JSONArray();

        JSONObject firstObject = new JSONObject();
        firstObject.put("delta_x", 250L);
        firstObject.put("delta_y", 80L);
        firstObject.put("x", 100L);
        firstObject.put("y", 100L);

        JSONObject secondObject = new JSONObject();
        secondObject.put("delta_x", 250L);
        secondObject.put("delta_y", 150L);
        secondObject.put("x", 120L);
        secondObject.put("y", 200L);

        jsonArray.add(firstObject);
        jsonArray.add(secondObject);

        List<IRectangle> rectangleList = JsonUtils.parseRectangleObject(jsonArray, 10);

        List<IRectangle> expectedRectangleList =
            JsonUtils.readJSONFile("json/no_intersection.json", FileType.RESOURCE, 10);

        assertEquals(expectedRectangleList, rectangleList);
    }

    @Test
    public void writeResultsToFile() throws IOException, ParseException {
        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/intersections_between_three_rectangles.json",
                FileType.RESOURCE, 10);

        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        StringBuilder sb = new StringBuilder("Input: ").append(System.lineSeparator());
        for (int i = 0; i < rectangleList.size(); i++) {
            sb.append("\t")
                .append(i + 1)
                .append(": ")
                .append("Rectangle at ")
                .append(rectangleList.get(i))
                .append(".")
                .append(System.lineSeparator());
        }

        sb.append(System.lineSeparator())
            .append("Intersections: ").append(System.lineSeparator());

        for (int i = 0; i < intersections.size(); i++) {
            IRectangle rectangle = intersections.get(i);
            sb.append("\t")
                .append(i + 1)
                .append(": ")
                .append(rectangle.intersectionsToString())
                .append(System.lineSeparator());
        }

        JsonUtils.writeResultsToFile(rectangleList, intersections);

        String fileName = "output.txt";

        StringBuilder expectedSb = new StringBuilder();
        try (FileInputStream fis = new FileInputStream(fileName);
             InputStreamReader isr = new InputStreamReader(fis,
                 StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)) {

            String line;

            while ((line = br.readLine()) != null) {
                expectedSb.append(line).append(System.lineSeparator());
            }
        }

        assertEquals(expectedSb.toString(), sb.toString());
    }

    @After
    public void tearDown() throws Exception {
        JsonUtils.writeResultsToFile(new ArrayList<>(), new ArrayList<>());
    }
}
