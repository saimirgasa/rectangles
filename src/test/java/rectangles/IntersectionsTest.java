package rectangles;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.json.simple.parser.ParseException;
import org.junit.Test;

import rectangles.dto.Point;
import rectangles.dto.Rectangle;
import rectangles.enums.FileType;
import rectangles.interfaces.IRectangle;
import rectangles.utils.JsonUtils;

public class IntersectionsTest {

    @Test
    public void findIntersections_NoRectangles() throws IOException, ParseException {
        List<IRectangle> rectangleList = JsonUtils.readJSONFile("json/no_rectangles.json", FileType.RESOURCE, 10);

        assertEquals(0, rectangleList.size());
    }

    @Test
    public void findIntersections_OneRectangles() throws IOException, ParseException {
        List<IRectangle> rectangleList = JsonUtils.readJSONFile("json/one_rectangle.json", FileType.RESOURCE, 10);
        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        assertEquals(1, rectangleList.size());
        assertEquals(0, intersections.size());
    }

    @Test
    public void findIntersections_NoIntersections() throws IOException, ParseException {
        List<IRectangle> rectangleList = JsonUtils.readJSONFile("json/no_intersection.json", FileType.RESOURCE, 10);
        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);
        assertEquals(0, intersections.size());
    }

    @Test
    public void findIntersections_ProcessOnlyTen() throws IOException, ParseException {
        List<IRectangle> rectangleList = JsonUtils.readJSONFile("json/more_than_ten_rectangles.json", FileType.RESOURCE, 10);

        assertEquals(10, rectangleList.size());
    }

    @Test
    public void findIntersections_WithOneIntersection() throws IOException, ParseException {
        List<IRectangle> rectangleList = JsonUtils.readJSONFile("json/one_intersection.json", FileType.RESOURCE, 10);
        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        assertEquals(1, intersections.size());
        assertEquals(40, intersections.get(0).getDeltaX());
        assertEquals(20, intersections.get(0).getDeltaY());
        assertEquals(310, intersections.get(0).getBottomLeft().getX());
        assertEquals(100, intersections.get(0).getBottomLeft().getY());
    }

    @Test
    public void findIntersections_SameTwoRectangles() throws IOException, ParseException {
        List<IRectangle> rectangleList = JsonUtils.readJSONFile("json/same_rectangles.json", FileType.RESOURCE, 10);
        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        assertEquals(2, rectangleList.size());
        assertEquals(0, intersections.size());
    }

    @Test
    public void findIntersections_MatchingIntersections() throws IOException, ParseException {
        List<IRectangle> rectangleList = JsonUtils.readJSONFile("json/matching_intersections.json", FileType.RESOURCE, 10);
        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        assertEquals(3, rectangleList.size());
        assertEquals(4, intersections.size());
    }

    @Test
    public void findIntersections_WithTwoTriangles() throws IOException, ParseException {
        List<IRectangle> rectangleList = JsonUtils.readJSONFile("json/intersections_between_two_rectangles.json", FileType.RESOURCE, 10);
        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        assertEquals(1, intersections.size());
        assertEquals(2, intersections.get(0).getIntersectingRectangles().size());
    }

    @Test
    public void findIntersections_WithThreeTriangles() throws IOException, ParseException {
        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/intersections_between_three_rectangles.json",
                FileType.RESOURCE, 10);

        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        IRectangle expectedRectangle = new Rectangle(
            99999999,
            new Point(160, 160),
            190,
            20,
            new TreeSet<>(rectangleList)
        );

        for (IRectangle intersection : intersections) {
            if (intersection.equals(expectedRectangle)) {
                assertEquals(3, intersection.getIntersectingRectangles().size());
            }
        }
    }

    @Test
    public void findIntersections_WithFourTriangles() throws IOException, ParseException {

        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/four_intersections/rectangles.json",
                FileType.RESOURCE, 10);

        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        List<IRectangle> expectedIntersectionsList =
            JsonUtils.readJSONFile("json/four_intersections/intersecting_rectangles.json",
                FileType.RESOURCE, 10);

        List<IRectangle> secondExpectedIntersectionsList = new ArrayList<>(expectedIntersectionsList);
        expectedIntersectionsList.remove(1);
        expectedIntersectionsList.remove(4);

        IRectangle expectedIntersection = new Rectangle(
            99999999,
            new Point(240, 160),
            50,
            20,
            new TreeSet<>(expectedIntersectionsList)
        );

        if (intersections.contains(expectedIntersection)) {
            assertEquals(4, intersections.get(intersections.indexOf(expectedIntersection)).getIntersectingRectangles().size());
        }

        secondExpectedIntersectionsList.remove(1);
        secondExpectedIntersectionsList.remove(3);

        expectedIntersection = new Rectangle(
            99999999,
            new Point(160, 160),
            40,
            20,
            new TreeSet<>(secondExpectedIntersectionsList)
        );

        if (intersections.contains(expectedIntersection)) {
            assertEquals(4, intersections.get(intersections.indexOf(expectedIntersection)).getIntersectingRectangles().size());
        }
    }

    @Test
    public void findIntersections_WithFiveTriangles() throws IOException, ParseException {

        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/five_intersections/rectangles.json",
                FileType.RESOURCE, 10);

        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        List<IRectangle> expectedIntersectionsList =
            JsonUtils.readJSONFile("json/five_intersections/intersecting_rectangles.json",
                FileType.RESOURCE, 10);

        expectedIntersectionsList.remove(0);
        expectedIntersectionsList.remove(3);

        IRectangle expectedIntersection = new Rectangle(
            99999999,
            new Point(180, 240),
            20,
            20,
            new TreeSet<>(expectedIntersectionsList)
        );

        if (intersections.contains(expectedIntersection)) {
            assertEquals(5, intersections.get(intersections.indexOf(expectedIntersection)).getIntersectingRectangles().size());
        }
    }
}
