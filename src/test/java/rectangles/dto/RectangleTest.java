package rectangles.dto;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.TreeSet;

import org.json.simple.parser.ParseException;
import org.junit.Test;

import rectangles.Intersections;
import rectangles.enums.FileType;
import rectangles.interfaces.IRectangle;
import rectangles.utils.JsonUtils;

public class RectangleTest {

    @Test
    public void getIntersectingRectangles() throws IOException, ParseException {
        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/intersections_between_three_rectangles.json",
                FileType.RESOURCE, 10);

        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);

        IRectangle expectedRectangle = new Rectangle(
            99999999,
            new Point(160, 160),
            190,
            20,
            new TreeSet<>(rectangleList)
        );

        TreeSet<IRectangle> expectedIntersectingRectangles = new TreeSet<>(rectangleList);

        assertEquals(expectedIntersectingRectangles, expectedRectangle.getIntersectingRectangles());
    }

    @Test
    public void intersectionsToString_TwoRectangles() throws IOException, ParseException {
        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/intersections_between_two_rectangles.json",
                FileType.RESOURCE, 10);

        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);
        String expectedString = "Between rectangle 1 and 3 at (100,100), delta_x=10, delta_y=10.";

        IRectangle intersection = intersections.get(0);
        assertEquals(expectedString, intersection.intersectionsToString());
    }

    @Test
    public void intersectionsToString_ThreeRectangles() throws IOException, ParseException {
        List<IRectangle> rectangleList =
            JsonUtils.readJSONFile("json/intersections_between_three_rectangles.json",
                FileType.RESOURCE, 10);

        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);
        String expectedString = "Between rectangle 1 and 2 at (140,160), delta_x=210, delta_y=20.\n" +
            "Between rectangle 1 and 3 at (160,140), delta_x=190, delta_y=40.\n" +
            "Between rectangle 2 and 3 at (160,160), delta_x=230, delta_y=100.\n" +
            "Between rectangle 1, 2 and 3 at (160,160), delta_x=190, delta_y=20.\n";

        StringBuilder sb = new StringBuilder();
        for (IRectangle intersection : intersections) {
            sb.append(intersection.intersectionsToString())
                .append(System.lineSeparator());
        }

        assertEquals(expectedString, sb.toString());
    }
}
