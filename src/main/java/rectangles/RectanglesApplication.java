package rectangles;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.json.simple.parser.ParseException;

import rectangles.enums.FileType;
import rectangles.interfaces.IRectangle;
import rectangles.utils.JsonUtils;

/**
 * This is a simple console application that takes in a
 * set of defined rectangles and finds and reports
 * the intersections between them.
 *
 * @author Saimir Gasa
 */
public class RectanglesApplication {

    private static final int NO_OF_TRIANGLES_TO_PROCESS = 10;
    private static final String INPUT_FILE_RESOURCE_PATH = "input.json";
    private static final String OUTPUT_FILE_PATH = "output.txt";

    public static void main(String[] args) throws IOException, ParseException
    {
        Scanner mainScanner = new Scanner(System.in);
        System.out.println("Hello! Please enter your name:");
        String name = mainScanner.nextLine();
        System.out.println("Hey " + name + "! Welcome to Intersecting Rectangles application!");
        System.out.println(name + ", this application takes as input a file with rectangle definitions.");
        System.out.println("The format of the JSON file should look something like this: ");
        System.out.println("{\n" +
            "    \"rects\": [\n" +
            "        {\"x\": 100, \"y\": 100, \"delta_x\": 250, \"delta_y\": 80 },\n" +
            "        {\"x\": 120, \"y\": 200, \"delta_x\": 250, \"delta_y\": 150 },\n" +
            "        {\"x\": 140, \"y\": 160, \"delta_x\": 250, \"delta_y\": 100 },\n" +
            "        {\"x\": 160, \"y\": 140, \"delta_x\": 350, \"delta_y\": 190 },\n" +
            "        {\"x\": 240, \"y\": 120, \"delta_x\": 50, \"delta_y\": 60 },\n" +
            "        {\"x\": 110, \"y\": 130, \"delta_x\": 90, \"delta_y\": 150 }\n" +
            "    ]\n" +
            "}");

        List<IRectangle> rectangleList;
        int exit = 3;
        int answer;
        boolean valid;
        do {
            System.out.println("Would you like to process this sample or provide your own file?");
            System.out.println("1. Process the sample file");
            System.out.println("2. Provide my own file");
            System.out.println("3. Exit the application");

            valid = true;
            try {
                answer = Integer.parseInt(mainScanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("INCORRECT INPUT: Please select one of the options.");
                do {
                    valid = false;
                    answer = -1;
                } while (valid);
            }
            if (answer == 1) {
                rectangleList = JsonUtils.readJSONFile(INPUT_FILE_RESOURCE_PATH, FileType.RESOURCE, NO_OF_TRIANGLES_TO_PROCESS);
                findIntersectionsAndWriteToOutputFile(rectangleList);
            } else if (answer == 2) {
                Scanner fileScanner = new Scanner(System.in);
                System.out.println("Please provide the absolute path of the triangle definitions file:");
                String absoluteFilePath = fileScanner.nextLine();
                try {
                    rectangleList = JsonUtils.readJSONFile(absoluteFilePath, FileType.EXTERNAL, NO_OF_TRIANGLES_TO_PROCESS);
                    findIntersectionsAndWriteToOutputFile(rectangleList);
                } catch (ParseException e) {
                    System.out.println("The application had a hard time parsing the input file provided. Could you please ensure content of the file is correct JSON format and try again?");
                } catch (FileNotFoundException e) {
                    System.out.println("The input file was not found in the path provided. Please make sure you provide the correct absolute path and try again.");
                }
            }
        } while (answer != exit);
        {
            System.out.println("Application exiting...");
        }
    }

    private static void findIntersectionsAndWriteToOutputFile(List<IRectangle> rectangleList)
    {
        List<IRectangle> intersections = Intersections.findIntersections(rectangleList);
        JsonUtils.writeResultsToFile(rectangleList, intersections);
        System.out.println("SUCCESS: Results were writen to '" + OUTPUT_FILE_PATH + "' file.");
    }
}
