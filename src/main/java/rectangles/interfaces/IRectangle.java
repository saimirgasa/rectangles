package rectangles.interfaces;

import java.util.Set;

import rectangles.dto.Point;

public interface IRectangle {
    int getId();
    Point getBottomLeft();
    int getDeltaX();
    int getDeltaY();
    Set<IRectangle> getIntersectingRectangles();
    String intersectionsToString();
}
