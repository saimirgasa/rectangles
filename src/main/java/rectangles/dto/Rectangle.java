package rectangles.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import rectangles.interfaces.IRectangle;

/**
 * The Rectangle class representing a rectangle.
 *
 * An individual Rectangle is defined by a {@link rectangles.dto.Point}
 * and x and y displacement from the bottom-left point defined.
 */
public class Rectangle implements IRectangle, Comparable<IRectangle> {

    private final int id;

    private final Point bottomLeft;

    private final int deltaX;
    private final int deltaY;

    private final Set<IRectangle> intersectingRectangles;

    public Rectangle(int id,
                     Point bottomLeft,
                     int deltaX,
                     int deltaY,
                     Set<IRectangle> intersectingRectangles) {

        this.id = id;
        this.bottomLeft = bottomLeft;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.intersectingRectangles = intersectingRectangles;
    }

    public int getId() {
        return id;
    }

    public Point getBottomLeft() {
        return bottomLeft;
    }

    public int getDeltaX() {
        return deltaX;
    }

    public int getDeltaY() {
        return deltaY;
    }

    public Set<IRectangle> getIntersectingRectangles() {
        return intersectingRectangles;
    }

    public String intersectionsToString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Between rectangle ");
        List<IRectangle> rectangleList = new ArrayList<>(intersectingRectangles);
        if (intersectingRectangles.size() == 2) {
            sb.append(rectangleList.get(0).getId())
                .append(" and ")
                .append(rectangleList.get(1).getId())
                .append(" at ")
                .append(this.toString())
                .append(".");
        } else {
            for (IRectangle rectangle : rectangleList) {
                int index = rectangleList.indexOf(rectangle);
                if (index == rectangleList.size() - 1) {
                    sb.append(" and ").append(rectangle.getId());
                } else if (index != rectangleList.size() - 2) {
                    sb.append(rectangle.getId()).append(", ");
                } else {
                    sb.append(rectangle.getId());
                }
            }
            sb.append(" at ")
                .append(this.toString())
                .append(".");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (deltaX != rectangle.deltaX) return false;
        if (deltaY != rectangle.deltaY) return false;
        if (!bottomLeft.equals(rectangle.bottomLeft)) return false;
        return intersectingRectangles.equals(rectangle.intersectingRectangles);
    }

    @Override
    public int hashCode() {
        int result = bottomLeft.hashCode();
        result = 31 * result + deltaX;
        result = 31 * result + deltaY;
        result = 31 * result + intersectingRectangles.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return bottomLeft +
            ", delta_x=" + deltaX +
            ", delta_y=" + deltaY;
    }

    @Override
    public int compareTo(IRectangle o) {
        return Integer.compare(id, o.getId());
    }
}
