package rectangles.dto;

import java.util.Set;

import rectangles.interfaces.IRectangle;

/**
 * This is a NullRectangle created only to avoid having to check for null.
 */
public class NullRectangle implements IRectangle {
    @Override
    public int getId() {
        return 0;
    }

    @Override
    public Point getBottomLeft() {
        return null;
    }

    @Override
    public int getDeltaX() {
        return 0;
    }

    @Override
    public int getDeltaY() {
        return 0;
    }

    @Override
    public Set<IRectangle> getIntersectingRectangles() {
        return null;
    }

    @Override
    public String intersectionsToString() {
        return null;
    }
}
