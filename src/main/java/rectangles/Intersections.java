package rectangles;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import rectangles.dto.NullRectangle;
import rectangles.dto.Point;
import rectangles.dto.Rectangle;
import rectangles.interfaces.IRectangle;

/**
 * The Intersections class.
 *
 * The Intersections class is responsible for calculating the intersections for given set of Rectangle objects.
 */
public class Intersections {

    private final int startCoord;
    private final int displacement;

    private Intersections(int startCoord, int displacement) {
        this.startCoord = startCoord;
        this.displacement = displacement;
    }

    private int getStartCoord() {
        return startCoord;
    }

    private int getDisplacement() {
        return displacement;
    }

    /**
     * This method calculates the displacement overlap between two coordinates. This method is used to find either
     * the horizontal or vertical displacement between the two coordinates.
     *
     * @param firstCoord The first point to compare. This could be x or y value of a {@link rectangles.dto.Point}.
     * @param firstDelta The first displacement from the first coordinate to compare. This could be deltaX or deltaY.
     * @param secondCoord The second point to compare. This could be x or y value of a {@link rectangles.dto.Point}.
     * @param secondDelta The second displacement from the second coordinate to compare. This could be deltaX or deltaY.
     * @return An instance of this class with the starting coordinate and its respective displacement.
     */
    private static Intersections findDisplacementOverlap(int firstCoord, int firstDelta, int secondCoord, int secondDelta) {

        int highestStartCoord = Math.max(firstCoord, secondCoord);
        int lowestEndCoord = Math.min(firstCoord + firstDelta, secondCoord + secondDelta);

        if (highestStartCoord >= lowestEndCoord) {
            return new Intersections(0, 0);
        }

        int overlapDisplacement = lowestEndCoord - highestStartCoord;

        return new Intersections(highestStartCoord, overlapDisplacement);
    }

    /**
     * This method calculates the rectangular overlap (intersection) between two rectangles.
     *
     * @param firstRectangle The first rectangle.
     * @param secondRectangle The second rectangle.
     * @return The intersecting rectangle or {@link rectangles.dto.NullRectangle} where there is no intersection.
     */
    private static IRectangle findIntersection(IRectangle firstRectangle, IRectangle secondRectangle) {

        Intersections xOverlap = findDisplacementOverlap(firstRectangle.getBottomLeft().getX(),
            firstRectangle.getDeltaX(),
            secondRectangle.getBottomLeft().getX(),
            secondRectangle.getDeltaX());

        Intersections yOverlap = findDisplacementOverlap(firstRectangle.getBottomLeft().getY(),
            firstRectangle.getDeltaY(),
            secondRectangle.getBottomLeft().getY(),
            secondRectangle.getDeltaY());

        if (xOverlap.getDisplacement() == 0 || yOverlap.getDisplacement() == 0) {
            return new NullRectangle();
        }

        if (firstRectangle.equals(secondRectangle)) {
            return new NullRectangle();
        }

        List<IRectangle> uniqueIntersections = new ArrayList<>(firstRectangle.getIntersectingRectangles());

        for (IRectangle rectangle : secondRectangle.getIntersectingRectangles()) {
            if (!uniqueIntersections.contains(rectangle)) {
                uniqueIntersections.add(rectangle);
            }
        }

        Set<IRectangle> intersections = new TreeSet<>();
        if (!uniqueIntersections.isEmpty()) {
            intersections.addAll(uniqueIntersections);
        } else {
            intersections.addAll(List.of(firstRectangle, secondRectangle));
        }

        return new Rectangle(
            99999999,
            new Point(xOverlap.getStartCoord(), yOverlap.getStartCoord()),
            xOverlap.getDisplacement(),
            yOverlap.getDisplacement(),
            intersections
        );
    }

    /**
     * This method finds all the intersections between the given rectangle list.
     *
     * @param rectangleList The rectangle list.
     * @return A list of all intersections represented in Rectangle objects.
     */
    public static List<IRectangle> findIntersections(List<IRectangle> rectangleList) {

        return findIntersectionsHelper(rectangleList, new ArrayList<>());
    }

    /**
     * This is a recursive call finding all the intersections between the given rectangle list.
     *
     * @param rectangleList The rectangle list.
     * @param totalIntersections The running total of the intersecting rectangles.
     * @return A list of all intersections represented in Rectangle objects.
     */
    private static List<IRectangle> findIntersectionsHelper(List<IRectangle> rectangleList,
                                                            List<IRectangle> totalIntersections) {

        List<IRectangle> tempIntersections = new ArrayList<>();

        if (!rectangleList.isEmpty()) {
            for (int i = 0; i < rectangleList.size(); i++) {
                IRectangle firstRectangle = rectangleList.get(i);
                for (int j = i + 1; j < rectangleList.size(); j++) {
                    IRectangle secondRectangle = rectangleList.get(j);

                    IRectangle intersection = findIntersection(firstRectangle, secondRectangle);
                    if (!(intersection instanceof NullRectangle)) {

                        if (totalIntersections.contains(intersection)) {
                            continue;
                        }
                        totalIntersections.add(intersection);
                        tempIntersections.add(intersection);
                    }
                }
            }
            return findIntersectionsHelper(tempIntersections, totalIntersections);
        }
        return totalIntersections;
    }
}
