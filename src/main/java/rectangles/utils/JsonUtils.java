package rectangles.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import rectangles.dto.Point;
import rectangles.dto.Rectangle;
import rectangles.enums.FileType;
import rectangles.interfaces.IRectangle;

/**
 * The FileUtils class.
 *
 * The FileUtils class parses the JSONArray read from the JSON file into a list of Rectangle objects.
 */
public class JsonUtils {

    private static final String OUTPUT_FILE_PATH = "output.txt";

    public JsonUtils() { }

    public static List<IRectangle> readJSONFile(String filePath,
                                                FileType fileType,
                                                int maxNumberOfTriangles) throws IOException, ParseException {

        JSONArray rectangleArray;
        FileReader fileReader;
        if (fileType == FileType.EXTERNAL) {
            fileReader = FileReader.newExternalFileReader(filePath);
        } else {
            fileReader = FileReader.newResourceFileReader(filePath);
        }
        rectangleArray = fileReader.readFile();
        if (rectangleArray.size() > maxNumberOfTriangles) {
            System.out.println("The '" + filePath + "' file contains " + rectangleArray.size() + " rectangles. Only the first 10 will be processed.");
        }

        return new ArrayList<>(JsonUtils.parseRectangleObject(rectangleArray, maxNumberOfTriangles));
    }

    public static List<IRectangle> parseRectangleObject(JSONArray rectangleArray, int maxNumberOfTriangles) {
        List<IRectangle> rectangles = new ArrayList<>();

        for (int i = 0; i < rectangleArray.size(); i++) {

            if (i < maxNumberOfTriangles) {
                JSONObject rectangle = (JSONObject) rectangleArray.get(i);
                //Get rectangle's x value
                int x = ((Long) rectangle.get("x")).intValue();

                //Get rectangle's y value
                int y = ((Long) rectangle.get("y")).intValue();

                //Get rectangle's deltaX
                int deltaX = ((Long) rectangle.get("delta_x")).intValue();

                //Get rectangle's deltaY
                int deltaY = ((Long) rectangle.get("delta_y")).intValue();

                rectangles.add(new Rectangle(i + 1, new Point(x, y), deltaX, deltaY, new HashSet<>()));
            }
        }
        return rectangles;
    }

    public static void writeResultsToFile(List<IRectangle> rectangleList,
                                          List<IRectangle> intersections) {

        try (FileWriter writer = new FileWriter(OUTPUT_FILE_PATH)) {
            StringBuilder sb = new StringBuilder("Input: ").append(System.lineSeparator());

            for (int i = 0; i < rectangleList.size(); i++) {
                sb.append("\t")
                    .append(i + 1)
                    .append(": ")
                    .append("Rectangle at ")
                    .append(rectangleList.get(i))
                    .append(".")
                    .append(System.lineSeparator());
            }

            sb.append(System.lineSeparator())
                .append("Intersections: ").append(System.lineSeparator());

            for (int i = 0; i < intersections.size(); i++) {
                IRectangle rectangle = intersections.get(i);
                sb.append("\t")
                    .append(i + 1)
                    .append(": ")
                    .append(rectangle.intersectionsToString())
                    .append(System.lineSeparator());
            }

            writer.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
