package rectangles.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * The FileReader class.
 *
 * The FileReader depending on the type of file can read from the absolute path or
 * from the resources directory within the project structure.
 */
public class FileReader {

    private InputStream inputStream;

    private FileReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public static FileReader newExternalFileReader(String filePath) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(filePath);
        return new FileReader(inputStream);
    }

    public static FileReader newResourceFileReader(String filePath) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        return new FileReader(classLoader.getResourceAsStream(filePath));
    }

    public JSONArray readFile() throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        assert this.inputStream != null;
        JSONObject rectanglesObject = (JSONObject) jsonParser.parse(
            new InputStreamReader(this.inputStream, StandardCharsets.UTF_8));

        return (JSONArray) rectanglesObject.get("rects");
    }
}
