# Rectangles

Rectangles is a simple console application which takes a JSON file defining a set of 
rectangles on a Cartesian coordinate grid. The application will report on the set of intersections created by 
the give rectangles.

Upon start up the user will be prompted to provide a JSON file with rectangle definitions or tell the application
to process a predefined set of rectangles included in the application.

## Prerequisites

* Have Java version 8 or later installed in your system.
* Have Maven version 3.6.x installed in your system.

## Installation

The user can run the application one of the two ways after cloning:

1. Running it in an IDE of their choice.
2. Building a jar file and executing the jar file.

#### IDE

* `git clone https://github.com/ai/size-limit.git <-- Change this to the correct value`
* Import the application to the IDE of your choice.
* Run the `RectanglesApplication` class.

#### Execute JAR file

* `git clone https://github.com/ai/size-limit.git <-- Change this to the correct value`
* In `terminal` change directory to the root directory of the project.
* Run `mvn clean package` to generate the application JAR file.
* Change directory to `target` by `cd target/`.
* Run `java -jar rectangles-1.0-SNAPSHOT-shaded.jar` command to start the application.

## Usage

Upon startup, the application will ask you for your name. This can be any character string. 

Then the application will greet you and have a simple explanation about the rectangle definition file.

A JSON file can be provided for the application to process. The application takes the 
absolute path of the JSON file and report the results in the `output.txt` file in the `target` directory.

The format of the JSON file should follow this format:

```
{
    "rects": [
        {"x": 100, "y": 100, "delta_x": 250, "delta_y": 80 },
        {"x": 120, "y": 200, "delta_x": 250, "delta_y": 150 },
        {"x": 140, "y": 160, "delta_x": 250, "delta_y": 100 },
        {"x": 160, "y": 140, "delta_x": 350, "delta_y": 190 },
        ...
        ...
        ...
    ]
}
```
* The `x` and `y` values represents the bottom-left point of the rectangle.
* The `delta_x` value represents the length of the rectangle starting from the bottom-left point.
* The `delta_y` value represents the height of the rectangle starting from the bottom-left point.

## Example

Let's take this JSON file as an example of an input file that defines the rectangles:

```
{
    "rects": [
        {"x": 100, "y": 100, "delta_x": 250, "delta_y": 80 },
        {"x": 120, "y": 200, "delta_x": 250, "delta_y": 150 },
        {"x": 140, "y": 160, "delta_x": 250, "delta_y": 100 },
        {"x": 160, "y": 140, "delta_x": 350, "delta_y": 190 }
    ]
}
```

The outcome should be the `output.txt` file that looks like:

```
Input:
        1: Rectangle at (100,100), delta_x=250, delta_y=80.
        2: Rectangle at (120,200), delta_x=250, delta_y=150.
        3: Rectangle at (140,160), delta_x=250, delta_y=100.
        4: Rectangle at (160,140), delta_x=350, delta_y=190.

Intersections:
        1: Between rectangle 1 and 3 at (140,160), delta_x=210, delta_y=20.
        2: Between rectangle 1 and 4 at (160,140), delta_x=190, delta_y=40.
        3: Between rectangle 2 and 3 at (140,200), delta_x=230, delta_y=60.
        4: Between rectangle 2 and 4 at (160,200), delta_x=210, delta_y=130.
        5: Between rectangle 3 and 4 at (160,160), delta_x=230, delta_y=100.
        6: Between rectangle 1, 3 and 4 at (160,160), delta_x=190, delta_y=20.
        7: Between rectangle 2, 3 and 4 at (160,200), delta_x=210, delta_y=60.
```
